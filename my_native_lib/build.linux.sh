cmake --version

# Setup temp build dir
mkdir -p build-linux
cd build-linux

# SRC dir
SRC_ROOT=$(dirname "$(pwd)")

# Configure
cmake .. \
    -DSRC_ROOT=$SRC_ROOT \
    -DCMAKE_INSTALL_PREFIX=$SRC_ROOT/out

# Compile
cmake --build . --target install --config Release

# Cleanup
cd ..
rm -Rf build-linux