@ECHO OFF
SETLOCAL EnableDelayedExpansion

IF "%CMAKE%" == "" (
	SET CMAKE="C:\Program Files\CMake\bin\cmake.exe"
) ELSE (
	ECHO CMAKE already defined as: %CMAKE%.
)

REM Setup temp build dir
IF EXIST build-win ( rmdir /Q /S  build-win )
mkdir build-win
cd build-win

REM Check if argument provided
if "%~1"=="" (
	SET SRC_ROOT=%CD%/..
) ELSE (
	SET SRC_ROOT="%~1"
)

REM Configure.
%CMAKE% .. ^
   -DSRC_ROOT=%SRC_ROOT% ^
   -DCMAKE_INSTALL_PREFIX=%SRC_ROOT%/out ^
   -G "Visual Studio 15 2017 Win64"

REM Compile.
%CMAKE%  --build . --target install --config Release


REM back and clean up
cd ..
IF EXIST build-win ( rmdir /Q /S  build-win )
