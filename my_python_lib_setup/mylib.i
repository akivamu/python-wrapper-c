%module(directors="1") mylib


%include "exception.i"
%exception
{ 
    try	{ $action }
	catch (std::exception& e) { SWIG_exception(SWIG_RuntimeError, e.what()); }
    catch (...)	{ SWIG_exception(SWIG_UnknownError, "unknown exception"); }
}

%{
    #define SWIG_FILE_WITH_INIT
    #define USE_SWIG
	#include "include/exported.h"
	#include "include/mylib_export.h"
%}

%include "include/exported.h"
%include "include/mylib_export.h"


/** Here are some reference links used for this file.

* scipy cookbook on SWIG-Python examples
  * https://scipy-cookbook.readthedocs.io/items/SWIG_NumPy_examples.html
* scipy doc reference and examples on SWIG
  * https://docs.scipy.org/doc/numpy/reference/swig.interface-file.html
  * https://docs.scipy.org/doc/numpy-1.13.0/reference/swig.interface-file.html
  * https://mit-crpg.github.io/OpenMOC/devguide/swig.html
* SWIG docs on Python
  * http://www.swig.org/Doc1.3/Python.html#Python_nn13
  * http://www.swig.org/Doc1.3/Arguments.html#Arguments_nn4
* On python module naming conventions
  * https://visualgit.readthedocs.io/en/latest/pages/naming_convention.html
*/
