# Prepare setup project
mkdir -p lib
cp -Rf ../my_native_lib/out/lib/*.so ./lib
mkdir -p include
cp -Rf ../my_native_lib/out/include/*.h ./include

# SWIG -------------------------------------------
# Generate swig python wrapper: src & py
swig -Wall -builtin -outdir . -c++ -python mylib.i

# swig wrapper py to target
cp -rf mylib.py my_python_lib

# Native lib
cp  -rf lib/*.so my_python_lib
