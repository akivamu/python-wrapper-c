# manylinux

PLAT=manylinux1_x86_64
set -e -x

# Compile wheels
for PYBIN in /opt/python/cp37*/bin; do
    "${PYBIN}/pip" install -r dev-requirements.txt
    "${PYBIN}/pip" wheel . -w wheelhouse/
done

export LD_LIBRARY_PATH=$(pwd)/my_python_lib:$LD_LIBRARY_PATH

# Bundle external shared libraries into the wheels
for whl in wheelhouse/*.whl; do
    auditwheel repair "$whl" --plat $PLAT -w wheel/
done
