@ECHO OFF

REM Prepare setup project
IF EXIST include (rmdir /Q /S include)
IF EXIST lib (rmdir /Q /S lib)
mkdir include
mkdir lib
COPY ..\my_native_lib\out\lib\*.dll .\lib
COPY ..\my_native_lib\out\lib\*.so .\lib
COPY ..\my_native_lib\out\lib\*.lib .\lib
COPY ..\my_native_lib\out\include\*.h .\include

REM SWIG -------------------------------------------
SET SWIG=C:\tool\swigwin\swigwin-4.0.1\swig.exe

REM Generate swig python wrapper: src & py
%SWIG% -Wall -builtin -outdir . -c++ -python mylib.i

REM Build ext
python setup.py build_ext --inplace

REM swig wrapper py to target
COPY mylib.py my_python_lib
COPY *.pyd my_python_lib

REM Native lib
COPY lib\*.so my_python_lib
COPY lib\*.dll my_python_lib

REM Build dist --------------------------------------
python setup.py sdist