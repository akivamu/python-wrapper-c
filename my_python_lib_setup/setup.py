#!/usr/bin/env python


"""
setup.py file
"""

from setuptools import setup, Extension
import numpy as np


mylib_module = Extension(
    name='_mylib',
    sources=['mylib_wrap.cxx'],
    include_dirs=['.', 'include', np.get_include()],
    libraries=['my_native_lib'],
    library_dirs=['lib'],
)

setup (name='my_python_lib',
    version='0.1.2',
    author='MyCompany',
    description='my_python_lib',
    ext_modules=[mylib_module],
    packages=['my_python_lib'],
    package_data={
        'my_python_lib': ['*.so', '*.dll', '*.pyd'],
    }
)
