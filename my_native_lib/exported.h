#pragma once

// Define MODULE_API for any platform
#if defined _WIN32 || defined __CYGWIN__
  #ifdef WIN_EXPORT
    // Exporting...
    #ifdef __GNUC__
      #define MODULE_API __attribute__ ((dllexport))
    #else
      #define MODULE_API __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
    #endif
  #else
    #ifdef __GNUC__
      #define MODULE_API __attribute__ ((dllimport))
    #else
      #define MODULE_API __declspec(dllimport) // Note: actually gcc seems to also supports this syntax.
    #endif
  #endif
  #define NOT_MODULE_API
#else
  #if __GNUC__ >= 4
    #define MODULE_API __attribute__ ((visibility ("default")))
    #define NOT_MODULE_API  __attribute__ ((visibility ("hidden")))
  #else
    #define MODULE_API
    #define NOT_MODULE_API
  #endif
#endif
