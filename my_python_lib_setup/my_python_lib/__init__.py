# Trick to load SO in linux
import platform
if platform.system().startswith('Linux'):
    import os
    import ctypes
    basedir = os.path.abspath(os.path.dirname(__file__))
    libpath = os.path.join(basedir, 'libmy_native_lib.so')
    dll = ctypes.CDLL(libpath)

from .mylib import *